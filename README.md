# :x: ErrorAPI

##### Start Date: 2023-12-01
##### Revision Date: 
##### Author: Thiago Yuji

## Objective

Offer an API for data validation, if any of the validation rules are violated, generate a report or throw an exception.

## Motivation

Validating an amount of data in a component can be complicated, especially if it's a large amount of data. This can easily turn the code into large blocks of if's and else's or into a POJO class with lots of annotations. It's usually not possible to generate a report with all the fields that have been violated.

This API will help validate an amount of data, if any of the rules are violated, generate a report or throw an exception.

## Resources

- Fields
- Predicates
- Report

## Actors

|Identificação|Descrição|
|-------------|---------|
|DEV      |Desenvolvedor|

## Use Cases

|Identificação|Descrição|Objetivo|
|-------------|---------|--------|
|[UC1](./usecases/UC1.md)|Investigate report|Investigate report with all invalid fields.|
|[UC2](./usecases/UC2.md)|Validate data|Validate if any rule were violated and throws an exception.|


```plantuml
:Developer: -left-> (Investigate report)
:Developer: -right-> (Validate data)
```

## Activities

```plantuml
start
:Receive fields;
:Validade fields;
:Generate report;
:Throw exception;
stop
```

## Modeling
- [Classes Diagram](./modeling/errorapi_classes.md)
- [Objects Diagram](./modeling/errorapi_objects.md)

## Example

```
    Fields
        .evaluate( T )
            .indentifiedAs( String )
            .messageOnError( String )
            .when( Predicate )
                .and( Predicate )
                .or( Predicate )
        .evaluate( S )
            .indentifiedAs( String )
            .messageOnError( String )
            .when( Predicate )
        ...
        .then()
        .report( action )
        .throws( Exception );
        
```