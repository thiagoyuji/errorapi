# Classes Diagram

##### Start Date: 2023-12-01
##### Revision Date: 
##### Author: Thiago Yuji

---

```plantuml
@startuml
interface Fields<O> {
    + {method} {static} evaluate( O ) : Fields<O>
}

interface Decision<O> {
    + {method} when( Predicate<O> ) : LogicalOperators<O>
}
Decision ..> LogicalOperators

interface LogicalOperators<O> {
    + {method} and( Predicate<O> ) : LogicalOperators<O>
    + {method} or( Predicate<O> ) : LogicalOperators<O>
}

interface Report {
    // uma parte do report acontece no inicio e outra no final, como manter a coesao ?
    // o que é identificacao e mensagem quando erro ? tem algum conceito por tras ?
    // tipicamente dados ...
    + {method} identifiedAs( String ) : 
    + {method} messageOnError( String ) : 
    + {method} report( action ) : void
    // é report que lança exeption?
    + {method} <E> throws( Class<E> ) : void
}
@enduml
```

```plantuml
@startuml
interface Evaluate<O> {
    + {method} {static} field( O ) : Evaluate<O>
    + {method} identified( String ) : EvaluateFinal<O>
}
Evaluate ..> EvaluateFinal

interface EvaluateFinal<O> {
    + {method} messageOnError( String ) : Decision<O>
}
EvaluateFinal ..> Decision

class EvaluateImpl<O> implements Evaluate, EvaluateFinal {
    - o : O
    - identifier : String
    - invalidEvaluates : List<EvaluateImpl>
    + {method} identified( String ) : EvaluateFinal<O>
    + {method} messageOnError( String ) : Decision<O>
}

interface Decision<O> {
    + {method} when( Predicate<O> ) : DecisionFinal<O>
}
Decision ..> DecisionFinal

interface DecisionFinal<O> {
    + {method} and( Predicate<O> ) : DecisionFinal<O>
    + {method} or( Predicate<O> ) : DecisionFinal<O>
    + {method} <NO> field( NO ) : Evaluate<NO>
    + {method} then() : Report
}
DecisionFinal ..> Report
DecisionFinal ..> Evaluate

class DecisionImpl <<InnerClassEvaluate>> implements Decision, DecisionFinal {
    - o : O
    - predicate : Predicate<O>
    + {method} when( Predicate<O> ) : DecisionFinal<O>
    + {method} and( Predicate<O> ) : DecisionFinal<O>
    + {method} or( Predicate<O> ) : DecisionFinal<O>
    + {method} then() : Report
    + {method} <NO> field( NO ) : Evaluate<NO>
}

interface Report {
    + {method} report( action ) : void
    + {method} <E> throws( Class<E> ) : void
}

class ReportImpl <<InnerClassEvaluate>> implements Report {
    - report : String
    + {method} report( action ) : void
    + {method} <E> throws( Class<E> ) : void
}
@enduml
```