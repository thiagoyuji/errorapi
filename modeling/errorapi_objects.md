# Classes Diagram

##### Start Date: 2023-12-01
##### Revision Date: 
##### Author: Thiago Yuji

---

## **Example for the Diagram**

> Consider the hypothetical idea that a person needs to be 21 years of age or older and have all government-approved documents to get their driver's license.

```
Evaluate
    .field(age)
        .identified("Age")
        .messageOnError("Age under 21 years old.")
        .when( (age) -> age < 21 )
    .field(documentsApproved)
        .identified("Documents Situation")
        .messageOnError("Documents rejected by goverment.")
        .when( (documentsApproved) -> !documentsApproved )
    .then()
    .report( (report) -> System.out.println(report) )
    .throws( new DriversLicenseRejected() )
```

```plantuml
@startuml
object EvaluateAge {
    o = 20
    identified = "Age"
    messageOnError = "Age under 21 years old."
    invalidEvaluates = []
}

object DecisionAge {
    o = 20
    predicate = (age) -> age < 21
}

object EvaluateDocumentSituation {
    o = false
    identified = "Documents Situation"
    messageOnError = "Documents rejected by goverment."
    invalidEvaluates = [EvaluateAge]
}

object DecisionAgeDocumentSituation {
    o = 20
    predicate = (age) -> age < 21
}

object Report {
    report = "{ "errors": [ {"field": "Age", ...}, {"field": "Document Situation", ...} ] }"
}

EvaluateAge --> DecisionAge
DecisionAge --> EvaluateDocumentSituation
EvaluateDocumentSituation --> DecisionAgeDocumentSituation
DecisionAgeDocumentSituation --> Report
@enduml
```