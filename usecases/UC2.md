# UC2 - Validate data

##### Start Date: 2023-12-01
##### Revision Date: 
##### Author: Thiago Yuji

## Description
Validate if any rule were violated and throws an exception.

## Actors
- Developer

## Conditions
#### **Preconditions:**
1. Receive fields and their respective identifiers to be evaluated.
2. Receive the predicate rules and messages in cases of invalid values.

## Flows
#### **Main Flow:**
1. The API receives fields, identifiers, predicates, and messages.
2. Evaluate all of the fields according to each rule.
3. Generate a report with all the errors.
4. Throw an exception if there's any error.

#### **Alternative Flow:**
1. Do not throw an exception, only generate a report.

## Business Rules
- The developer must provide the necessary information to the API.

## Sequences Diagram

```plantuml
@startuml
actor Developer
participant ErrorAPI

Developer -> ErrorAPI : Request Field Validation
activate ErrorAPI

ErrorAPI -> ErrorAPI : Validate Fields

ErrorAPI --> Developer : Send Error Report
ErrorAPI -> Developer : Throw Exception
deactivate ErrorAPI

@enduml
```

## Metadata

#### **Metadata JSON Input:**

```
    Fields
        .evaluate( T )
            .indentifiedAs( String )
            .messageOnError( String )
            .when( Predicate )
                .and( Predicate )
                .or( Predicate )
        .evaluate( S )
            .indentifiedAs( String )
            .messageOnError( String )
            .when( Predicate )
        ...
        .then()
        .report( action )
        .throws( Exception );
```

#### **Metadata JSON Report:**

```json
{
    "errors": [
        {"field": "Age", "message": "Age under 21 years old."},
        {"field": "Documents Situation", "message": "Documents rejected by goverment."}
        // ... Other errors
    ]
}
```

#### **Metadata JSON Exception:**
###### *New fields defined by actor*

```
- code
- message
```