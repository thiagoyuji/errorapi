# UC1 - Investigate Report

##### Start Date: 2023-12-01
##### Revision Date: 
##### Author: Thiago Yuji

## Description
Investigate report with all invalid fields.

## Actors
- Developer

## Flows
#### **Main flow:**
1. The API evaluates fields and generates a report with the invalid fields found.
2. The developer receives the report and investigates it. 

## Business Rules

- Only invalid fields will be in the report after validation.

## Sequences Diagram

```plantuml
@startuml
actor Developer
participant ErrorAPI

Developer -> ErrorAPI : Request Field Validation
ErrorAPI --> Developer : Send Report for Investigation

@enduml
```

## Metadata

#### **Metadata JSON Output:**

```json
{
    "errors": [
        {"field": "Age", "message": "Age under 21 years old."},
        {"field": "Documents Situation", "message": "Documents rejected by goverment."}
        // ... Other errors
    ]
}
```